# FossBot - a bot for Matrix that converts links and does other stuff
# Copyright (C) 2022 Zortazert and other contributors

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from matrix_bot_api.matrix_bot_api import MatrixBotAPI
from matrix_bot_api.mregex_handler import MRegexHandler
from matrix_bot_api.mcommand_handler import MCommandHandler
import requests
import json
import re
import tempfile
import random

# Matrix account information must be stored in a file called
# user-pass.json
with open('user-pass.json', 'r') as f:
    user = json.load(f)

USERNAME = user["Username"]  # Bot's username
PASSWORD = user["Password"]  # Bot's password
SERVER = user["Server"]  # Matrix server URL

# Put the instances of the libre services you want to use here. Put
# only the domain name here, without the protocol, e.g. "yewtu.be" and
# not "https://yewtu.be"
INVIDIOUS_INSTANCE = "inv.vern.cc"
NITTER_INSTANCE = "nt.vern.cc"
TEDDIT_INSTANCE = "lr.vern.cc"

def help_callback(room, event):
    room.send_text("""FossBot is a matrixbot that can do multiple things:

- Detect sent youtube, reddit or twitter links and send back a message link with a proxy site such as: invidious, teddit and nitter.

Commands:
- !fbh or !help
Print out this help message.
- !fby QUERY or !invidious QUERY
Print out YouTube search results as Invidious links with title and channel information. Uses the Invidious API
- !fbp QUERY or !peertube QUERY
Print out PeerTube search results as PeerTube instance links with title and channel information. Uses the Sepia Search API
- !fbl QUERY or !lbry QUERY
Print out LBRY search results as librarian links with title and channel information. Uses the lighthouse API
- !fbc QUERY or !crypto QUERY <number of><currency>
Print out in USD values of crypto currencies. Uses the rate.sx API.
- !fbn QUERY or !neow QUERY
Print out the amount of neow coins a user on neow
- !fbw QUERY or !wikipedia QUERY
Print out wikipedia search result snippets of different articles including links to those articles.
- !fbU QUERY or !uncyclopedia QUERY
Print out uncyclopedia search result snippets of different articles including links to those articles.
- !fbu QUERY or !urban QUERY
Print out urban dictionary search result descriptions and examples of slang words.
- !fbg QUERY or !git QUERY
Print out repository information for GitHub and Gitea projects.
- !fbd QUERY or !dictionary QUERY
Print out dictionary defintion of a word
- !fbt QUERY or !tenor QUERY
Send out tenor gifs. Uses the tenor API.
- !fbs QUERY or !search QUERY
Print out search results of a search. Uses the librex API.
- !fbm QUERY or !meme QUERY
Print out origins/descriptions of the QUERY meme. Scrapes knowyourmeme.
- !fbrg QUERY = !garfield
Sends a random garfield.""")
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def garfield_callback(room,event):
    if event["content"]["body"].startswith("!fbrg ") == True:
        query = event["content"]["body"].replace("!fbm ","")
    if event["content"]["body"].startswith("!garfield ") == True:
        query = event["content"]["body"].replace("!garfield ","")
    else:
        query = re.sub("^!fbrg|\[(.*?)\]: !fbrg ", "", event["content"]["body"])
    try:
        url = f'https://www.gocomics.com/{datetime.datetime.strftime(query, "%Y/%m/%d")}'
    except:
        url = "https://www.gocomics.com/random/garfield"
    print(url)
    data = requests.get(url, allow_redirects=True).text
    loc = data.find('https://assets.amuniversal.com/')
    image = data[loc:loc+63]
    temp_dir = tempfile.TemporaryDirectory().name
    with open(temp_dir, 'wb') as file:
        file.write(requests.get(image).content)
    with open(temp_dir, 'rb') as file:
        bot = MatrixBotAPI(USERNAME, PASSWORD, SERVER)
        upload = bot.client.upload(file, "image/png")
        room.send_image(upload, f"garfield_meme.gif")


def knowyourmeme_callback(room,event):
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20121201 icecat/17.0.1'}
    if event["content"]["body"].startswith("!fbm ") == True:
        query = event["content"]["body"].replace("!fbm ","")
    if event["content"]["body"].startswith("!meme ") == True:
        query = event["content"]["body"].replace("!meme ","")
    else:
        query = re.sub("^!fbm|\[(.*?)\]: !fbm ", "", event["content"]["body"])

    room.send_text(f"Searching knowyourmeme for {query}")
    search = f"https://knowyourmeme.com/search?q={query}"
    search_result = requests.get(search, headers=headers)
    search_data = str(search_result.content)
    if "No results" in search_data:
        room.send_text("Nothing found :(")
        quit()

    links = re.findall('<a href="/memes/.+?">',search_data)
    for i, link in enumerate(links[15:]):
        url = "https://knowyourmeme.com" + link.replace("<a href=\"", "").replace("\">", "").replace("\" rel=\"nofollow","")
        if "trending" in url:
            break
        elif i == 2:
            break
        try:
            selected = "https://knowyourmeme.com"+links[i+18].replace("<a href=\"", "").replace("\">", "").replace("\" rel=\"nofollow","")

            result = requests.get(selected, headers=headers)
            data = str(result.content)

            paragraph = re.findall('<p>(.+?)</p>',data)
            cleaner = re.compile('<.*?>')
            #about = re.sub(cleaner, '', paragraph[2])
            origin = re.sub(cleaner, '', paragraph[3])
            #room.send_text("About:" + about.replace("\\'","'"))
            room.send_text(origin.replace("\\'","'"))
        except:
            room.send_text("Some error probably")
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def librex_callback(room, event):
    if event["content"]["body"].startswith("!fbs ") == True:
        query = event["content"]["body"].replace("!fbs ","")
    if event["content"]["body"].startswith("!search ") == True:
        query = event["content"]["body"].replace("!search ","")
    else:
        query = re.sub("^!fbs|\[(.*?)\]: !fbs ", "", event["content"]["body"])
    room.send_text(f"Searching librex for {query}")
    librex_instance = "https://search.davidovski.xyz/"
    librex_search = librex_instance + "api.php?q=" + query + "&p=1&type=0&invidious=https://invidio.xamh.de&nitter=https://nitter.net&reddit=https://teddit.net"
    print(librex_search)

    data = requests.get(librex_search)
    json_stuff = json.loads(data.text)
    for i, search in enumerate(json_stuff):
        room.send_text(search["title"]+"\n"+search["url"].replace("www.reddit.com",TEDDIT_INSTANCE).replace("reddit.com",TEDDIT_INSTANCE))
        if i == 2:
            break
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def tenor_callback(room, event):
    if event["content"]["body"].startswith("!fbt ") == True:
        query = event["content"]["body"].replace("!fbt ","")
    if event["content"]["body"].startswith("!tenor ") == True:
        query = event["content"]["body"].replace("!tenor ","")
    else:
        query = re.sub("^!fbt|\[(.*?)\]: !fbt ", "", event["content"]["body"])

    #headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20121201 icecat/17.0.1'}
    #search = f"https://tenor.com/search/{query}-gifs"
    #search_result = requests.get(search, headers=headers)
    #search_data = str(search_result.content)

    #links = re.findall('<img src=.+?>',search_data)
    #if "No Results" in search_data:
    #    room.send_text("ERROR: Nothing found :(")
    #else:
    #    r = random.randint(0,9)
    #    url = re.findall(r'(https?://\S+)', links[33+r])
    #    url = url[0].replace("\"", "")
    #    print(url)
    #    #room.send_text(f"<img src=\"{url}\">")

    #    temp_dir = tempfile.TemporaryDirectory().name
    #    thumbnail_data = requests.get(url)
    key = "LIVDSRZULELA"
    size = str(10)
    search = f"https://g.tenor.com/v1/search?q={query}&key={key}&limit={size}"
    data = requests.get(search)
    json_stuff = json.loads(data.text)
    if json_stuff["next"] == "0":
        room.send_text("Nothing found :(")
    elif query == "":
        room.send_text("ERROR: use !fbt QUERY or !tenor QUERY")
    else:
        try:
            r = random.randint(0,10)
            print(json_stuff["results"][r]["media"][0]["mediumgif"]["url"])
            temp_dir = tempfile.TemporaryDirectory().name
            thumbnail_data = requests.get(json_stuff["results"][r]["media"][0]["mediumgif"]["url"])
        except:
            r = 0
            print(json_stuff["results"][r]["media"][0]["mediumgif"]["url"])
            temp_dir = tempfile.TemporaryDirectory().name
            thumbnail_data = requests.get(json_stuff["results"][r]["media"][0]["mediumgif"]["url"])

        with open(temp_dir, 'wb') as file:
            file.write(thumbnail_data.content)
        with open(temp_dir, 'rb') as file:
            bot = MatrixBotAPI(USERNAME, PASSWORD, SERVER)
            upload = bot.client.upload(file, "image/png")
            room.send_image(upload, f"tenor_meme.gif")
        room.send_text(str(json_stuff["results"][r]["media"][0]["mediumgif"]["url"]))
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def dictionary_callback(room, event):
    if event["content"]["body"].startswith("!fbd ") == True:
        query = event["content"]["body"].replace("!fbd ","")
    if event["content"]["body"].startswith("!dictionary ") == True:
        query = event["content"]["body"].replace("!dictionary ","")
    else:
        query = re.sub("^!fbd|\[(.*?)\]: !fbd ", "", event["content"]["body"])
    data = requests.get(f"https://api.dictionaryapi.dev/api/v2/entries/en/{query}").json()

    try:
        definition = data[0]["meanings"][0]["definitions"][0]["definition"]
    except:
        definition = "No definitions found."
    room.send_text(f"""Searching the Dictionary for {query}:
{definition}""")
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def urban_callback(room, event):
    if event["content"]["body"].startswith("!fbu ") == True:
        message = event["content"]["body"].replace("!fbu ","")
    if event["content"]["body"].startswith("!urban ") == True:
        message = event["content"]["body"].replace("!urban ","")
    else:
        message = re.sub("^!fbu|\[(.*?)\]: !fbu ", "", event["content"]["body"])
    api = f"https://api.urbandictionary.com/v0/define?term={message}"
    data = requests.get(api)
    json_stuff = json.loads(data.text)
    room.send_text(f"Searching urban dictionary for {message}")
    for i, result in enumerate(json_stuff["list"]):
        room.send_text(result["word"]+"\n"+result["definition"][:128].replace("\n","").replace("[","").replace("]","")+"..."+"\n"+result["example"][:128].replace("\n","").replace("[","").replace("]","")+"...")
        if i == 1:
            break
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def git_callback(room, event):
    if event["content"]["body"].startswith("!fbg ") == True:
        message = event["content"]["body"].replace("!fbg ","")
    if event["content"]["body"].startswith("!git ") == True:
        message = event["content"]["body"].replace("!git ","")
    else:
        message = re.sub("^!fbg|\[(.*?)\]: !fbg ", "", event["content"]["body"])
    query = message.split("/")
    if len(query) == 3:
        room.send_text("ERROR: Please add a repo name to your url.")
    elif query[2] == "github.com":
        github_api = f"https://api.github.com/repos/{query[3]}/{query[4]}"
        data = requests.get(github_api)
        json_stuff = json.loads(data.text)
        mb_size= round(float(json_stuff["size"]) / 1024, 1)
        if json_stuff["license"] == None:
            licence = "No Licence"
        else:
            licence = json_stuff["license"]["name"]

        languages = []
        data_languages = requests.get(f"https://api.github.com/repos/{query[3]}/{query[4]}/languages").json()
        for key in data_languages:
            languages.append(key)
        room.send_text(f"""Repo Name: {json_stuff["name"]}
Author Name: {json_stuff["owner"]["login"]}
Size: {mb_size} mb
Licence: {licence}
Branch: {json_stuff["default_branch"]}
Issues: {json_stuff["open_issues"]}
Languages: {', '.join(languages)}""")
    else:
        try:
            gitea_api = f"https://{query[2]}/api/v1/repos/{query[3]}/{query[4]}"
            data = requests.get(gitea_api)
            json_stuff = json.loads(data.text)
            mb_size= round(float(json_stuff["size"]) / 1024, 1)
            languages=[]
            data_languages = requests.get(f"https://{query[2]}/api/v1/repos/{query[3]}/{query[4]}/languages").json()
            for key in data_languages:
                languages.append(key)
            room.send_text(f"""Repo Name: {json_stuff["name"]}
Author Name: {json_stuff["owner"]["username"]}
Branch: {json_stuff["default_branch"]}
Size: {mb_size} mb
Issues: {json_stuff["open_issues_count"]}
Pull Requests: {json_stuff["open_pr_counter"]}
Languages: {', '.join(languages)}""")
        except:
            room.send_text("ERROR: Most likely this link isn't a gitea instance link or a github link. At the moment only gitea websites with gitea api support and github links are supported.")


    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def wikipedia_callback(room, event):
    if event["content"]["body"].startswith("!fbw ") == True:
        message = event["content"]["body"].replace("!fbw ","")
    if event["content"]["body"].startswith("!wikipedia ") == True:
        message = event["content"]["body"].replace("!wikipedia ","")
    else:
        message = re.sub("^!fbw|\[(.*?)\]: !fbw ", "", event["content"]["body"])
    query = message
    wikipedia_api = f"https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch={query}&format=json"
    data = requests.get(wikipedia_api)
    json_stuff = json.loads(data.text)
    room.send_text(f"Searching wikipedia for {query}")

    for index, post in enumerate(json_stuff["query"]["search"]):
        clean = re.compile('<.*?>')
        room.send_text(re.sub(clean, '', post["title"]+"\n"+post["snippet"].replace("&quot;","\"")+"\n"+"https://en.wikipedia.org/w/index.php?curid="+str(post["pageid"])))
        if index == 2:
            break
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def uncyclopedia_callback(room, event):
    if event["content"]["body"].startswith("!fbU ") == True:
        message = event["content"]["body"].replace("!fbU ","")
    if event["content"]["body"].startswith("!uncyclopedia ") == True:
        message = event["content"]["body"].replace("!uncyclopedia ","")
    else:
        message = re.sub("^!fbU|\[(.*?)\]: !fbU ", "", event["content"]["body"])
    query = message
    wikipedia_api = f"https://en.uncyclopedia.co/w/api.php?action=query&list=search&srsearch={query}&format=json"
    data = requests.get(wikipedia_api)
    json_stuff = json.loads(data.text)
    room.send_text(f"Searching Uncyclopedia for {query}")

    for index, post in enumerate(json_stuff["query"]["search"]):
        clean = re.compile('<.*?>')
        room.send_text(re.sub(clean, '', post["title"]+"\n"+post["snippet"]+"\n"+"https://en.uncyclopedia.co/w/index.php?curid="+str(post["pageid"])))
        if index == 2:
            break
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def neow_callback(room, event):
    if event["content"]["body"].startswith("!fbn ") == True:
        message = event["content"]["body"].replace("!fbn ","")
    if event["content"]["body"].startswith("!neow ") == True:
        message = event["content"]["body"].replace("!neow ","")
    else:
        message = re.sub("^!fbn|\[(.*?)\]: !fbn ", "", event["content"]["body"])
    query = message
    neow_search = "https://neow.matthewevan.xyz/user/" + query + "/neowcoins.txt"
    data = requests.get(neow_search).text.rstrip()
    room.send_text(query + " has " + data + " coins")
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def crypto_callback(room, event):
    if event["content"]["body"].startswith("!fbc ") == True:
        message = event["content"]["body"].replace("!fbc ","")
    if event["content"]["body"].startswith("!crypto ") == True:
        message = event["content"]["body"].replace("!crypto ","")
    else:
        message = re.sub("^!fbc|\[(.*?)\]: !fbc ", "", event["content"]["body"])
    query = message
    ratesx_search = "https://rate.sx/" + query
    data = requests.get(ratesx_search).text.rstrip()
    if str(query).isalpha() == True:
        room.send_text("ERROR: Must specify <number of><currency>")
    else:
        room.send_text("In USD that's " + data)
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def peertube_callback(room, event):
    if event["content"]["body"].startswith("!fbp ") == True:
        message = event["content"]["body"].replace("!fbp ","")
    if event["content"]["body"].startswith("!peertube ") == True:
        message = event["content"]["body"].replace("!peertube ","")
    else:
        message = re.sub("^!fbp|\[(.*?)\]: !fbp ", "", event["content"]["body"])
    query = message
    peertube_search = "https://sepiasearch.org/api/v1/search/videos?search=" + query
    data = requests.get(peertube_search)
    json_stuff = json.loads(data.text)
    room.send_text("Searching PeerTube for " + query)
    for index, vid in enumerate(json_stuff["data"]):
        room.send_text(vid["name"]+"\n"+vid["channel"]["displayName"]+"\n"+vid["url"])
        if index == 2:
            break
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def invidious_callback(room, event):
    if event["content"]["body"].startswith("!fby ") == True:
        message = event["content"]["body"].replace("!fby ","")
    if event["content"]["body"].startswith("!invidious ") == True:
        message = event["content"]["body"].replace("!invidious ","")
    else:
        message = re.sub("^!fby|\[(.*?)\]: !fby ", "", event["content"]["body"])
    query = message
    invidious_search = "https://invidio.xamh.de/api/v1/search?q=" + query
    data = requests.get(invidious_search)
    json_stuff = json.loads(data.text)
    room.send_text("Searching YouTube for " + query)
    for index, vid in enumerate(json_stuff):
        room.send_text(vid["title"]+"\n"+vid["author"]+"\n"+"https://" + INVIDIOUS_INSTANCE + "/watch?v="+vid["videoId"])
        if index == 2:
            break
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])
def lbry_callback(room, event):
    size = str(3)
    if event["content"]["body"].startswith("!fbl ") == True:
        message = event["content"]["body"].replace("!fbl ","")
    if event["content"]["body"].startswith("!lbry ") == True:
        message = event["content"]["body"].replace("!lbry ","")
    else:
        message = re.sub("^!fbl|\[(.*?)\]: !fbl ", "", event["content"]["body"])
    query = message
    search = 'https://lighthouse.lbry.com/search?s=' + query + '&include=channel,title,&size=' + size

    data = requests.get(search)
    json_stuff = json.loads(data.text)

    room.send_text("Searching the LBRY network for: " + query)
    for x in json_stuff:
        pre = "https://lbry.ix.tc/"
        if x["channel"]:
            pre += x["channel"] + "/"
        url = pre + x["name"]
        room.send_text(x["title"]+"\n"+url)
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def youtube_callback(room, event):
    message = event["content"]["body"]
    if "youtu.be" in message:
        room.send_text("Hi, " + event['sender'] + " here is an Invidious link of the YouTube link you just sent:\n"+message.replace("youtu.be",INVIDIOUS_INSTANCE))
    else:
        room.send_text("Hi, " + event['sender'] + " here is an Invidious link of the YouTube link you just sent:\n"+message.replace("youtube.com",INVIDIOUS_INSTANCE))

    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def twitter_callback(room, event):
    message = event["content"]["body"]
    if "mobile" in message:
        message = message.replace("mobile.twitter.com",NITTER_INSTANCE)
    else:
        message = message.replace("twitter.com",NITTER_INSTANCE)
    room.send_text("Hi, " + event['sender'] + " here is a nitter link of the Twitter link you just sent:\n"+message)
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def reddit_callback(room, event):
    message = event["content"]["body"]
    if "www.reddit.com" in message:
        room.send_text("Hi, " + event['sender'] + " here is a teddit link of the Reddit link you just sent:\n"+message.replace("www.reddit.com",TEDDIT_INSTANCE))
    else:
        room.send_text("Hi, " + event['sender'] + " here is a teddit link of the Reddit link you just sent:\n"+message.replace("reddit.com",TEDDIT_INSTANCE))
    print(event['sender']+"\nPosted:\n"+event["content"]["body"])

def main():
    # Create an instance of the MatrixBotAPI
    bot = MatrixBotAPI(USERNAME, PASSWORD, SERVER)

    youtube_com_handler = MRegexHandler("^((?:https?:)?//)?((?:www|m).)?((?:youtube(-nocookie)?.com|youtu.be))(/(?:[\w-]+?v=|embed/|v/)?)([\w-]+)(\S+)?$", youtube_callback)
    bot.add_handler(youtube_com_handler)
    #youtube_com_handler = MRegexHandler("https://youtube.com/", youtube_callback)
    #bot.add_handler(youtube_com_handler)
    #youtube_handler = MRegexHandler("https://youtu.be/", youtube_callback)
    #bot.add_handler(youtube_handler)
    #youtube_www_handler = MRegexHandler("https://www.youtube.com/", youtube_callback)
    #bot.add_handler(youtube_www_handler)
    twitter_handler = MRegexHandler("https://mobile.twitter.com/|https://twitter.com/", twitter_callback)
    bot.add_handler(twitter_handler)
    reddit_handler = MRegexHandler("https://reddit.com/", reddit_callback)
    bot.add_handler(reddit_handler)
    www_reddit_handler = MRegexHandler("https://www.reddit.com/", reddit_callback)
    bot.add_handler(www_reddit_handler)
    invidious_handler = MRegexHandler("^!invidious", invidious_callback)
    bot.add_handler(invidious_handler)
    invidious_abrev_handler = MRegexHandler("^!fby|\[(.*?)\]: !fby", invidious_callback)
    bot.add_handler(invidious_abrev_handler)
    lbry_handler = MRegexHandler("^!lbry", lbry_callback)
    bot.add_handler(lbry_handler)
    lbry_abrev_handler = MRegexHandler("^!fbl|\[(.*?)\]: !fbl", lbry_callback)
    bot.add_handler(lbry_abrev_handler)
    crypto_handler = MRegexHandler("^!crypto", crypto_callback)
    bot.add_handler(crypto_handler)
    crypto_abrev_handler = MRegexHandler("^!fbc|\[(.*?)\]: !fbc", crypto_callback)
    bot.add_handler(crypto_abrev_handler)
    neow_handler = MRegexHandler("^!neow", neow_callback)
    bot.add_handler(neow_handler)
    neow_abrev_handler = MRegexHandler("^!fbn|\[(.*?)\]: !fbn", neow_callback)
    bot.add_handler(neow_abrev_handler)
    help_abrev_handler = MRegexHandler("^!fbh|\[(.*?)\]: !fbh", help_callback)
    bot.add_handler(help_abrev_handler)
    help_handler = MRegexHandler("^!help", help_callback)
    bot.add_handler(help_handler)
    peertube_handler = MRegexHandler("^!peertube", peertube_callback)
    bot.add_handler(peertube_handler)
    peertube_abrev_handler = MRegexHandler("^!fbp|\[(.*?)\]: !fbp", peertube_callback)
    bot.add_handler(peertube_abrev_handler)
    wikipedia_handler = MRegexHandler("^!wikipedia", wikipedia_callback)
    bot.add_handler(wikipedia_handler)
    wikipedia_abrev_handler = MRegexHandler("^!fbw|\[(.*?)\]: !fbw", wikipedia_callback)
    bot.add_handler(wikipedia_abrev_handler)
    uncyclopedia_handler = MRegexHandler("^!uncyclopedia", uncyclopedia_callback)
    bot.add_handler(uncyclopedia_handler)

    uncyclopedia_abrev_handler = MRegexHandler("^!fbU|\[(.*?)\]: !fbU", uncyclopedia_callback)
    bot.add_handler(uncyclopedia_abrev_handler)
    git_handler = MRegexHandler("^!git", git_callback)
    bot.add_handler(git_handler)
    git_abrev_handler = MRegexHandler("^!fbg|\[(.*?)\]: !fbg", git_callback)
    bot.add_handler(git_abrev_handler)
    urban_handler = MRegexHandler("^!urban", urban_callback)
    bot.add_handler(urban_handler)
    urban_abrev_handler = MRegexHandler("^!fbu|\[(.*?)\]: !fbu", urban_callback)
    bot.add_handler(urban_abrev_handler)
    dictionary_handler = MRegexHandler("^!dictionary", dictionary_callback)
    bot.add_handler(dictionary_handler)
    dictionary_abrev_handler = MRegexHandler("^!fbd|\[(.*?)\]: !fbd", dictionary_callback)
    bot.add_handler(dictionary_abrev_handler)
    tenor_abrev_handler = MRegexHandler("^!fbt|\[(.*?)\]: !fbt", tenor_callback)
    bot.add_handler(tenor_abrev_handler)
    tenor_handler = MRegexHandler("^!tenor", tenor_callback)
    bot.add_handler(tenor_handler)
    search_abrev_handler = MRegexHandler("^!fbs|\[(.*?)\]: !fbs", librex_callback)
    bot.add_handler(search_abrev_handler)
    search_handler = MRegexHandler("^!search", librex_callback)
    bot.add_handler(search_handler)
    meme_abrev_handler = MRegexHandler("^!fbm|\[(.*?)\]: !fbm", knowyourmeme_callback)
    bot.add_handler(meme_abrev_handler)
    meme_handler = MRegexHandler("^!meme", knowyourmeme_callback)
    bot.add_handler(meme_handler)
    garfield_abrev_handler = MRegexHandler("^!fbrg|\[(.*?)\]: !fbrg", garfield_callback)
    bot.add_handler(garfield_abrev_handler)
    garfield_handler = MRegexHandler("^!garfield", garfield_callback)
    bot.add_handler(garfield_handler)
    bot.start_polling()
    # Infinitely read stdin to stall main thread while the bot runs in other threads
    while True:
        input()

def main_loop():
    while True:
        try:
            main()
            break
        except Exception as err:
            print(err)
            continue
main_loop()
