# FossBot

Fossbot is a [Matrix](https://matrix.org/) bot that can detect when a sent message contains a link to a proprietary tracking website (currently YouTube, Reddit, and Twitter), and reply with a link to a libre frontend (like Invidious, Teddit, and Nitter). It can also do various other things, see the commands below. It makes use of [python-matrix-bot-api plugin](https://github.com/shawnanastasio/python-matrix-bot-api).

## Extra Commands:

- `!fbh` or `!help` <br>
Print out this help message.
- `!fby QUERY` or `!invidious QUERY`<br>
Print out YouTube search results as Invidious links with title and channel information. Uses the Invidious API
- `!fbp QUERY` or `!peertube QUERY` <br>
Print out PeerTube search results as PeerTube instance links with title and channel information. Uses the Sepia Search API
- `!fbl QUERY` or `!lbry QUERY` <br>
Print out LBRY search results as librarian links with title and channel information. Uses the lighthouse API
- `!fbc QUERY` or `!crypto QUERY` `<number of><currency>` <br>
Print out in USD values of crypto currencies. Uses the rate.sx API.
- `!fbn QUERY` or `!neow QUERY` <br>
Print out the amount of neow coins a user on [neow](https://neow.matthewevan.xyz) has.
- `!fbw QUERY` or `!wikipedia QUERY` <br>
Print out wikipedia search result snippets of different articles including links to those articles.
- `!fbU QUERY` or `!uncyclopedia QUERY` <br>
Print out uncyclopedia search result snippets of different articles including links to those articles.
- `!fbu QUERY` or `!urban QUERY` <br>
Print out urban dictionary search result descriptions and examples of slang words.
- `!fbg QUERY` or `!git QUERY`<br>
Print out repository information for GitHub and Gitea projects.
- `!fbd QUERY` or `!dictionary QUERY` <br>
Print out dictionary defintion of a word
- `!fbt QUERY` or `!tenor QUERY` <br>
Send out tenor gifs.
- `!fbs QUERY` or `!search QUERY` <br>
Print out search results of a search. Uses the librex API.
- `!fbm QUERY` or `!meme QUERY` <br>
Print out origins/descriptions of the QUERY meme. Scrapes knowyourmeme.
- `!fbrg` QUERY = `!garfield` <br>
Sends a random garfield.

# Hosting FossBot

Here is what you will need to do host fossbot yourself:
- Create a Matrix account and save the login credentials.
- Making sure you have python and pip installed run `pip install matrix_bot_api`
- Either download the script by running this in your terminal `curl -o fossbot.py https://codeberg.org/zortazert/FossBot/raw/branch/main/fossbot.py` **or** do `git clone https://codeberg.org/zortazert/FossBot`
- Create a new JSON file called `user-pass.json` and fill out information related to your matrix account created for the bot:

```json
{
    "Username": "@user:host",
    "Password": "12345",
    "Server": "host"
}
```
- Run the script, preferably on a server that runs constantly or just run the script on a computer you have access to on your own schedule.

Another option is to contact me at the links listed below for me to make the account **I** run fossbot on inside your server. It isn't guaranteed I will put him in your server. Also I run fossbot only during the CST daytime, not 24/7.

# Help is Appreciated!

I am not a great programmer, yet. Please write bug reports, suggestions for the inclusion of new ideas/commands, and code improvements!

# Contact me

- Email: zortazert@matthewevan.xyz
- Matrix: @trueauracoral:tchncs.de

# Watch FossBot in action!

[![Video explination](https://tube.tchncs.de/lazy-static/previews/99b24a26-17f6-4abb-b914-0684514c9b8f.jpg)](https://tube.tchncs.de/w/tGPUDpuP3k5QZzJHyaDWn4)

Click on the image above to watch the video ^ or just click this PeerTube link. https://tube.tchncs.de/w/tGPUDpuP3k5QZzJHyaDWn4

Librarian: https://lbry.mutahar.rocks/@trueauracoral:a/fossbot:6

**NOTE:** This video shows a older version of fossbot with a bit less commands and different syntax to use the bot. This video is purely for showcasing the bot.

# License

Fossbot is libre software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

FossBot is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Contributors

Here is a list of contributors to Fossbot. If you contribute you can add your name here. See the git log for more details on contributions.

- Zortazert (Author; details above)
- Vertbyqb (Matrix contact: `@vertbyqb:tchncs.de`)
